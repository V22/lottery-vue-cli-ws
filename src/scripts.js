(function () {
    "use strict";

    var input = document.getElementById('number'),
        inputValue,
        label = input.parentElement.children[0],
        infoBlock = document.getElementsByClassName('info')[0],
        btnSendNumber = document.getElementById('btnSendNumber'),
        btnCheckNumber = document.getElementById('btnCheckNumber'),
        table = document.getElementById('tablePlayers'),
        tableBody = document.getElementById('tablePlayersBody'),
        winNumbersBlock = document.getElementById('winNumbers'),
        arrWinNumbers = [],
        arrNumbers = [],
        myObj = {
            name: 'You',
            numbers: []
        },
        myCurrentNumber = 1,
        myNumbersMax = 10,
        winNumbersMax = 12,
        minValue = 1,
        maxValue = 99,
        prize = 1000,
        prizeCount = 0,
        prizeItem,
        arrPlayers;


    //set label text
    function set_label() {
        return label.innerHTML= 'my number ' + myCurrentNumber + ' of ' + myNumbersMax;
    };
    set_label();

    //search in array
    function in_array(value, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] === value) return true;
        }
        return false;
    }

    //random number
    function randomNumbers(){
        return Math.floor(Math.random()*(maxValue-minValue+1)+minValue);
    }

    //reset win prize
    function resetWinPrize(){
        for (var j = 0; j < arrPlayers.length; j++) {
            if(arrPlayers[j].winNumCount>0){
                arrPlayers[j].winPrize = prizeItem*arrPlayers[j].winNumCount;
                tableBody.rows[j].children[2].innerHTML =  arrPlayers[j].winPrize;
            }
        }
    }

    //get players from json
    function getPlayers(){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.myjson.com/bins/thvld', true);
        xhr.send();
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            if (this.status != 200) {
                alert( xhr.status + ': ' + xhr.statusText );
            } else {
                var getJSON = xhr.responseText;
                arrPlayers = JSON.parse(getJSON);
                arrPlayers.unshift(myObj);
                for (var i = 0; i < arrPlayers.length; i++) {
                    tableBody.innerHTML += '<tr><th scope="row">' + arrPlayers[i].name + '</th>' + '<td><span>' + arrPlayers[i].numbers.join('</span> <span>') + '</span></td><td class="text-success"></td></tr>';
                    arrPlayers[i].winNumCount = 0;
                    arrPlayers[i].winPrize = 0;
                }
            }
        }
    }


    //btn - send my number
    btnSendNumber.onclick = function () {
        inputValue = input.value;
        if (in_array(parseInt(inputValue), myObj.numbers)) {
            alert('this number already exist');
        }else if(inputValue<minValue||inputValue>maxValue) {
            alert('range from 1 to 99');
        }else{
            myObj.numbers.push(parseInt(inputValue));
            ++myCurrentNumber;
            set_label();
            if(myCurrentNumber===myNumbersMax+1) {
                input.disabled = true;
                btnSendNumber.disabled = true;
                label.innerHTML= 'done';
                btnCheckNumber.disabled = false;
                getPlayers();
            }
        }
    }


    //btn - start lottery
    btnCheckNumber.onclick = function () {

        winNumbersBlock.innerHTML = 'Win numbers: ';

        function setRandomNumbers(){
            if(arrWinNumbers.length===winNumbersMax){
                infoBlock.innerHTML = 'You win: ' + arrPlayers[0].winPrize + ' $';
                infoBlock.style.display = 'block';
                clearInterval(setRandomNumbers);
                return false;
            }
            var randNum = randomNumbers();
            var index;
            arrWinNumbers.push(randNum);
            for(var i = 0; i < arrPlayers.length; i++){
                index = arrPlayers[i].numbers.indexOf(randNum);
                if (index!==-1) {
                    arrPlayers[i].winNumCount++;
                    prizeCount++;
                    tableBody.rows[i].children[1].children[index].className = "text-success";
                    prizeItem = Math.floor(prize/prizeCount);
                    resetWinPrize();
                }
            }
            return winNumbersBlock.innerHTML += randNum + ' ';
        }
        setInterval(setRandomNumbers, 1000);
    }


})();



// [
//     {
//         "name": "John",
//         "numbers": [
//             1,
//             2,
//             3,
//             4,
//             5,
//             6,
//             7,
//             8,
//             9,
//             10
//         ]
//     },
//     {
//         "name": "Steve",
//         "numbers": [
//             11,
//             12,
//             13,
//             14,
//             15,
//             16,
//             17,
//             18,
//             19,
//             20
//         ]
//     },
//     {
//         "name": "Alex",
//         "numbers": [
//             31,
//             32,
//             33,
//             34,
//             35,
//             36,
//             37,
//             38,
//             39,
//             40
//         ]
//     },
//     {
//         "name": "Michael",
//         "numbers": [
//             41,
//             42,
//             43,
//             44,
//             45,
//             46,
//             47,
//             48,
//             49,
//             50
//         ]
//     },
//     {
//         "name": "Gor",
//         "numbers": [
//             51,
//             52,
//             53,
//             54,
//             55,
//             56,
//             57,
//             58,
//             59,
//             60
//         ]
//     },
//     {
//         "name": "Bob",
//         "numbers": [
//             61,
//             62,
//             63,
//             64,
//             65,
//             66,
//             67,
//             68,
//             69,
//             70
//         ]
//     },
//     {
//         "name": "Sem",
//         "numbers": [
//             71,
//             72,
//             73,
//             74,
//             75,
//             76,
//             77,
//             78,
//             79,
//             80
//         ]
//     },
//     {
//         "name": "James",
//         "numbers": [
//             81,
//             82,
//             83,
//             84,
//             85,
//             86,
//             87,
//             88,
//             89,
//             90
//         ]
//     },
//     {
//         "name": "Paul",
//         "numbers": [
//             91,
//             92,
//             93,
//             94,
//             95,
//             96,
//             97,
//             98,
//             99,
//             1
//         ]
//     },
//     {
//         "name": "Rocky",
//         "numbers": [
//             10,
//             20,
//             30,
//             40,
//             50,
//             60,
//             70,
//             80,
//             90,
//             99
//         ]
//     }
// ]
